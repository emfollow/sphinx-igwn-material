Material Sphinx Theme for IGWN
==============================


**License**

|MIT License|

A Material Design theme for Sphinx documentation. Based on
`Material for MkDocs <https://squidfunk.github.io/mkdocs-material/>`_,
and `Guzzle Sphinx Theme <https://github.com/guzzle/guzzle_sphinx_theme>`_
and `Kevin Sheppard Theme <https://github.com/bashtage/sphinx-material>`_

See the theme's `demonstration site <https://bashtage.github.io/sphinx-material/>`_
for examples of rendered rst.

Installation
------------

Install via pip:

.. code-block:: bash

    $ pip install sphinx-igwn-material

or if you have the code checked out locally:

.. code-block:: bash

    $ pip install .

Configuration
-------------

Add the following to your conf.py:

.. code-block:: python

    html_theme = 'sphinx_igwn_material'


There are a lot more ways to customize this theme, as this more comprehensive
example shows:

.. code-block:: python

    # Required theme setup
    html_theme = 'sphinx_material'

    # Set link name generated in the top bar.
    html_title = 'Project Title'

    # Material theme options (see theme.conf for more information)
    html_theme_options = {

        # Set the name of the project to appear in the navigation.
        'nav_title': 'Project Name',

        # Set you GA account ID to enable tracking
        'google_analytics_account': 'UA-XXXXX',

        # Specify a base_url used to generate sitemap.xml. If not
        # specified, then no sitemap will be built.
        'base_url': 'https://project.github.io/project',

        # Set the color and the accent color
        'color_primary': 'blue',
        'color_accent': 'light-blue',

        # For colors with custom hex values, set e.g.:
        'color_primary': 'custom',
        'color_accent': 'custom',
        'custom_color_primary': '546e7a',
        'custom_color_accent': '448aff',

        # Set the repo location to get a badge with stats
        'repo_url': 'https://github.com/project/project/',
        'repo_name': 'Project',

        # Visible levels of the global TOC; -1 means unlimited
        'globaltoc_depth': 3,
        # If False, expand all TOC entries
        'globaltoc_collapse': False,
        # If True, show hidden TOC entries
        'globaltoc_includehidden': False,
    }

Customizing the layout
----------------------

You can customize the theme by overriding Jinja template blocks. For example,
'layout.html' contains several blocks that can be overridden or extended.

Place a 'layout.html' file in your project's '/_templates' directory.

.. code-block:: bash

    mkdir source/_templates
    touch source/_templates/layout.html

Then, configure your 'conf.py':

.. code-block:: python

    templates_path = ['_templates']

Finally, edit your override file 'source/_templates/layout.html':

::

    {# Import the theme's layout. #}
    {% extends '!layout.html' %}

    {%- block extrahead %}
    {# Add custom things to the head HTML tag #}
    {# Call the parent block #}
    {{ super() }}
    {%- endblock %}
