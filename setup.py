from setuptools import setup

setup(
    name="sphinx_igwn_material",
    version="1.0",
    description="Material sphinx theme for IGWN",
    long_description=open("README.rst").read(),
    packages=["sphinx_igwn_material"],
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=[
        "sphinx>=2.0",
        "beautifulsoup4",
        "python-slugify[unidecode]",
        "css_html_js_minify",
    ],
    license="MIT",
    entry_points={
        "sphinx.html_themes": [
            "sphinx_igwn_material = sphinx_igwn_material",
        ]
    },
)
